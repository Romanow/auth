package ru.romanow.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.*;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.security.SocialUserDetailsService;
import ru.romanow.auth.bitbucket.BitbucketConnectionFactory;
import ru.romanow.auth.dropbox.DropboxApi;
import ru.romanow.auth.dropbox.DropboxConnectionFactory;
import ru.romanow.auth.spring.SimpleConnectionSignUp;
import ru.romanow.auth.spring.SimpleSocialUsersDetailService;
import ru.romanow.auth.yahoo.YahooConnectionFactory;

/**
 * Created by ronin on 28.09.16
 */
@Configuration
@EnableSocial
@PropertySources({
        @PropertySource(value = "file:/Users/ronin/Develop/JAVA/oauth.properties", ignoreResourceNotFound = true),
})
public class OAuthSecurityConfiguration
        extends SocialConfigurerAdapter {

    @Value("${dropbox.app.key}")
    private String dropboxAppKey;

    @Value("${dropbox.security.key}")
    private String dropboxSecurityKey;

    @Value("${bitbucket.app.key}")
    private String bitbucketAppKey;

    @Value("${bitbucket.security.key}")
    private String bitbucketSecurityKey;

    @Value("${yahoo.app.key}")
    private String yahooAppKey;

    @Value("${yahoo.security.key}")
    private String yahooSecurityKey;

    @Autowired
    private InMemoryUserDetailsManager userDetailsService;

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
        connectionFactoryConfigurer.addConnectionFactory(new DropboxConnectionFactory(dropboxAppKey, dropboxSecurityKey));
        connectionFactoryConfigurer.addConnectionFactory(new BitbucketConnectionFactory(bitbucketAppKey, bitbucketSecurityKey));
        connectionFactoryConfigurer.addConnectionFactory(new YahooConnectionFactory(yahooAppKey, yahooSecurityKey));
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        InMemoryUsersConnectionRepository connectionRepository = new InMemoryUsersConnectionRepository(connectionFactoryLocator);
        connectionRepository.setConnectionSignUp(connectionSignUp());
        return connectionRepository;
    }

    @Bean
    public SocialUserDetailsService socialUsersDetailService() {
        return new SimpleSocialUsersDetailService(userDetailsService);
    }

    @Bean
    public ConnectionSignUp connectionSignUp() {
        return new SimpleConnectionSignUp(userDetailsService);
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public DropboxApi dropbox(ConnectionRepository repository) {
        Connection<DropboxApi> connection = repository.findPrimaryConnection(DropboxApi.class);
        return connection != null ? connection.getApi() : null;
    }
}
