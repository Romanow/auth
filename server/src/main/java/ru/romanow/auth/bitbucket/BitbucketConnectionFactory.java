package ru.romanow.auth.bitbucket;

import org.springframework.social.connect.support.OAuth1ConnectionFactory;

/**
 * Created by ronin on 29.09.16
 */
public class BitbucketConnectionFactory
        extends OAuth1ConnectionFactory<BitbucketApi> {

    public BitbucketConnectionFactory(String appId, String appSecret) {
        super("bitbucket", new BitbucketServiceProvider(appId, appSecret), new BitbucketApiAdapter());
    }
}
