package ru.romanow.auth.bitbucket;

import org.springframework.social.oauth1.AbstractOAuth1ApiBinding;
import org.springframework.social.support.URIBuilder;

import java.net.URI;

/**
 * Created by ronin on 29.09.16
 */
public class BitbucketTemplate
        extends AbstractOAuth1ApiBinding
        implements BitbucketApi {
    private static final String BITBUCKET_ENDPOINT = "https://api.bitbucket.org";

    public BitbucketTemplate(String consumerKey, String consumerSecret, String accessToken, String accessTokenSecret) {
        super(consumerKey, consumerSecret, accessToken, accessTokenSecret);
    }

    @Override
    public BitbucketUserInfo userInfo() {
        URI uri = URIBuilder.fromUri(BITBUCKET_ENDPOINT + "/2.0/user").build();
        return getRestTemplate().getForObject(uri, BitbucketUserInfo.class);
    }
}
