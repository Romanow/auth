package ru.romanow.auth.bitbucket;

import com.google.common.base.MoreObjects;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by ronin on 29.09.16
 */
@Data
@Accessors(chain = true)
public class BitbucketUserInfo {
    private String uuid;
    private String username;
    private String website;
    private String displayName;
}
