package ru.romanow.auth.dropbox;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * Created by ronin on 29.09.16
 */
public class DropboxApiAdapter
    implements ApiAdapter<DropboxApi> {

        @Override
    public boolean test(DropboxApi api) {
        return true;
    }

    @Override
    public void setConnectionValues(DropboxApi api, ConnectionValues values) {}

    @Override
    public UserProfile fetchUserProfile(DropboxApi api) {
        DropboxUserInfo userInfo = api.userInfo();
        return new UserProfile(userInfo.getAccount_id(), userInfo.getEmail(), "", "", "", "");
    }

    @Override
    public void updateStatus(DropboxApi api, String message) {

    }
}
