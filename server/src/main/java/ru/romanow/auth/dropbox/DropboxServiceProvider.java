package ru.romanow.auth.dropbox;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * Created by ronin on 28.09.16
 */
public class DropboxServiceProvider
        extends AbstractOAuth2ServiceProvider<DropboxApi> {

    public DropboxServiceProvider(String appId, String appSecret) {
        super(getOAuth2Template(appId, appSecret));
    }

    private static OAuth2Template getOAuth2Template(String appId, String appSecret) {
        OAuth2Template oAuth2Template = new OAuth2Template(
                appId, appSecret,
                "https://www.dropbox.com/1/oauth2/authorize",
                "https://api.dropboxapi.com/1/oauth2/token");
        oAuth2Template.setUseParametersForClientAuthentication(true);
        return oAuth2Template;
    }

    @Override
    public DropboxApi getApi(String accessToken) {
        return new DropboxTemplate(accessToken);
    }
}
