package ru.romanow.auth.dropbox;

import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.support.URIBuilder;

import java.net.URI;

/**
 * Created by ronin on 28.09.16
 */
public class DropboxTemplate
        extends AbstractOAuth2ApiBinding
        implements DropboxApi {
    private static final String DROPBOX_ENDPOINT = "https://api.dropboxapi.com";

    public DropboxTemplate(String accessToken) {
        super(accessToken);
    }

    @Override
    public DropboxUserInfo userInfo() {
        URI uri = URIBuilder.fromUri(DROPBOX_ENDPOINT + "/2/users/get_current_account").build();
        return getRestTemplate().postForObject(uri, null, DropboxUserInfo.class);
    }
}
