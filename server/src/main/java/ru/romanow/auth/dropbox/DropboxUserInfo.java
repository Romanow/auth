package ru.romanow.auth.dropbox;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by ronin on 28.09.16
 */
@Data
@Accessors(chain = true)
public class DropboxUserInfo {
    private String account_id;
    private String email;
}
