package ru.romanow.auth.yahoo;

/**
 * Created by romanow on 04.10.16
 */
public interface YahooApi {

    YahooUserInfo userInfo();
}
