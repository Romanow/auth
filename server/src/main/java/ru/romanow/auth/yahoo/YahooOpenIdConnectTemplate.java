package ru.romanow.auth.yahoo;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.social.oauth2.OAuth2Template;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * Created by romanow on 04.10.16
 */
public class YahooOpenIdConnectTemplate
        extends OAuth2Template {
    private final String clientId;
    private final String authorizeUrl;

    public YahooOpenIdConnectTemplate(String clientId, String clientSecret, String authorizeUrl, String accessTokenUrl) {
        super(clientId, clientSecret, authorizeUrl, accessTokenUrl);

        this.clientId = clientId;
        this.authorizeUrl = authorizeUrl;
    }

    @Override
    public String buildAuthenticateUrl(OAuth2Parameters parameters) {
   		return buildAuthUrl(authorizeUrl, parameters);
   	}

    @Override
   	public String buildAuthenticateUrl(GrantType grantType, OAuth2Parameters parameters) {
        return buildAuthUrl(authorizeUrl, parameters);
   	}

    @Override
    protected AccessGrant createAccessGrant(String accessToken, String scope, String refreshToken, Long expiresIn, Map<String, Object> response) {
        return new OpenIdConnectAccessGrant(accessToken, scope, refreshToken, expiresIn, (String)response.get("id_token"));
    }

    private String buildAuthUrl(String baseAuthUrl, OAuth2Parameters parameters) {
        String url = null;
        try {
            URIBuilder builder = new URIBuilder(baseAuthUrl);
            builder.addParameter("response_type", "code");
            builder.addParameter("nonce", RandomStringUtils.randomAlphanumeric(5));
            builder.addParameter("client_id", clientId);
            builder.addParameter("scope", "openid,sdpp-w");
            for (Map.Entry<String, List<String>> param : parameters.entrySet()) {
                for (final String value : param.getValue()) {
                    builder.addParameter(param.getKey(), value);
                }
            }

            url = builder.build().toString();
        } catch (URISyntaxException ignored) {}

        return url;
    }
}
